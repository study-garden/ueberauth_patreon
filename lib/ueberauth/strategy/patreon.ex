defmodule Ueberauth.Strategy.Patreon do
  @moduledoc """
  Provides an Ueberauth strategy for authentication with Patreon.
  """

  use Ueberauth.Strategy,
    uid_field: :id,
    default_scope:
      "identity identity[email] identity.memberships campaigns campaigns.members campaigns.members[email]"

  alias Ueberauth.Auth.Info
  alias Ueberauth.Auth.Credentials
  alias Ueberauth.Auth.Extra

  def handle_request!(conn) do
    scopes = conn.params["scope"] || option(conn, :default_scope)

    opts =
      [scope: scopes]
      |> Keyword.put(:redirect_uri, callback_url(conn))
      |> with_state_param(conn)

    redirect!(conn, Ueberauth.Strategy.Patreon.OAuth.authorize_url!(opts))
  end

  @doc """
  Handle callbacks from Patreon.
  """
  def handle_callback!(%Plug.Conn{params: %{"code" => code}} = conn) do
    opts = [redirect_uri: callback_url(conn)]
    token = Ueberauth.Strategy.Patreon.OAuth.get_token!([code: code], opts)

    if token.access_token == nil do
      err = token.other_params["error"]
      desc = token.other_params["error_description"]
      set_errors!(conn, [error(err, desc)])
    else
      conn
      |> store_token(token)
      |> fetch_user(token)
    end
  end

  def handle_cleanup!(conn) do
    conn
    |> put_private(:patreon_user, nil)
    |> put_private(:patreon_token, nil)
  end

  @doc """
  Fetches the uid field from the response.
  """
  def uid(conn) do
    uid_field =
      conn
      |> option(:uid_field)
      |> to_string

    conn.private.patreon_user["data"][uid_field]
  end

  def credentials(conn) do
    token = conn.private.patreon_token
    scopes = split_scopes(token)

    %Credentials{
      token: token.access_token,
      refresh_token: token.refresh_token,
      token_type: Map.get(token, :token_type),
      expires: !!token.expires_at,
      expires_at: token.expires_at,
      scopes: scopes
    }
  end

  def info(conn) do
    user = conn.private.patreon_user

    %Info{
      # name: get_in(user, ["data"]["attributes"]["full_name"]),
      # first_name: get_in(user, ["data"]["attributes"]["first_name"]),
      # last_name: get_in(user, ["data"]["attributes"]["last_name"]),
      # campaign.vanity for creators, nil if not a creator
      # nickname: campaign["vanity"],
      email: user["data"]["attributes"]["email"],
      # we don't need this, but it's retrievable from user.address
      # location: user.address.(lots of info)
      # description: get_in(user, ["data"]["attributes"]["about"]),
      # image: get_in(user, ["data"]["attributes"]["thumb_url"]),
      # phone: user.address.phone_number
      # no birthdays in Patreon
      # birthday: binary | nil,
      urls: %{
        # user info
        user_url: user["links"]["self"]
        # user_image_url: get_in(user, ["data"]["attributes"]["image_url"]),
        # user_thumb_url: get_in(user, ["data"]["attributes"]["thumb_url"])
        # campaign info
        # campaign_url: campaign["url"],
        # campaign_image_url: campaign["image_url"],
        # campaign_image_small_url: campaign["image_small_url"],
        # campaign_pledge_url: campaign["pledge_url"]
        # member_info
      }
    }
  end

  @doc """
  Stores the raw information (including the token) obtained from the Patreon callback.
  """
  def extra(conn) do
    %Extra{
      raw_info: %{
        user: conn.private.patreon_user,
        token: conn.private.patreon_token
      }
    }
  end

  defp store_token(conn, token) do
    put_private(conn, :patreon_token, token)
  end

  defp fetch_user(conn, token) do
    path = "https://www.patreon.com/api/oauth2/v2/identity"

    case Ueberauth.Strategy.Patreon.OAuth.get(token, path) do
      {:ok, %OAuth2.Response{status_code: 401, body: _body}} ->
        set_errors!(conn, [error("token", "unauthorized")])

      {:ok, %OAuth2.Response{status_code: status_code, body: user}}
      when status_code in 200..399 ->
        put_private(conn, :patreon_user, user)

      {:error, %OAuth2.Response{status_code: status_code}} ->
        set_errors!(conn, [error("OAuth2", status_code)])

      {:error, %OAuth2.Error{reason: reason}} ->
        set_errors!(conn, [error("OAuth2", reason)])
    end
  end

  # inspired by ueberauth_discord
  # https://github.com/schwarz/ueberauth_discord
  defp fetch_campaigns(%Plug.Conn{assigns: %{ueberauth_failure: _fails}} = conn, _), do: conn

  defp fetch_campaigns(conn, token) do
    scopes = split_scopes(token)

    case "campaigns" in scopes do
      false ->
        conn

      true ->
        path = "https://www.patreon.com/api/oauth2/v2/campaigns"

        case Ueberauth.Strategy.Patreon.OAuth.get(token, path) do
          {:ok, %OAuth2.Response{status_code: 401, body: _body}} ->
            set_errors!(conn, [error("token", "unauthorized")])

          {:ok, %OAuth2.Response{status_code: status_code, body: campaigns}}
          when status_code in 200..399 ->
            put_private(conn, :patreon_campaigns, campaigns)

          {:error, %OAuth2.Error{reason: reason}} ->
            set_errors!(conn, [error("OAuth2", reason)])
        end
    end
  end

  defp split_scopes(token) do
    (token.other_params["scope"] || "")
    |> String.split(" ")
  end

  defp option(conn, key) do
    Keyword.get(options(conn), key, Keyword.get(default_options(), key))
  end
end
