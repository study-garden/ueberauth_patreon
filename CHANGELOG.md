# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.11] - 2023-03-28
### Changed
- Upgrade Elixir to 1.14
- Upgrade Ueberauth to 0.10.5

### Fixed
- Add case clause for OAuth2.Response errors

## [0.1.10] - 2022-10-22
### Fixed
- Get correct user ID from Patreon.

## [0.1.9] - 2022-10-22
### Fixed
- Fix token storage in `handle_callback!` function.

## [0.1.8] - 2022-10-22
### Fixed
- Fix OAuth2 client config. 

## [0.1.7] - 2022-10-22
### Fixed
- Fix `handle_callback!`. 

## [0.1.6] - 2022-10-22
### Fixed
- Correct token retrieval parameters.
- Correct user information retrieval parameters.

## [0.1.5] - 2022-10-22
### Fixed
- Fix default scope to be correctly delimited and parsed.

## [0.1.4] - 2022-10-22
### Fixed
- Correct site for OAuth2 client.
- Correct dates for versions 0.1.2 and 0.1.3.

## [0.1.3] - 2022-10-22
### Added
- `config.exs` file.

### Fixed
- Correct GitLab URL for source code repository.

## [0.1.2] - 2022-10-22
### Added
- Ueberauth Patreon strategy.
- OAuth Patreon strategy.

## [0.1.1] - 2022-10-15
### Fixed
- Remove "Hello World" test.

## [0.1.0] - 2022-10-15
### Added
- Create README file with basic project metadata and installation instructions.
- Create LICENSE file with BSD 3-Clause and copyright information.
- Define `ueberauth` and `ExDoc` as a dependencies.
